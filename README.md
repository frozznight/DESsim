DESsim
========================================
Set of Python modules for the simulation of District Energy Systems and networks.

Current modules include:
* Weather: translating weather files (EPW and French RT2012 database) into a standard tabular format;
* Geometry: creation of a 3D model from a 2.5D vector GIS model and calculation of the sky view maps for each building surface using the POV-Ray (Cason [1994] 2013) simulation engine;
* Solar: calculation of the PV solar potential based on the PVLIB library (William Holmgren, Hansen, and Mikofski 2018; Will Holmgren et al. 2019);
* Climelioth: a dynamic building performance simulation tool based on the French RT2012 norm;
* GeoCAD: sizing the district heating & cooling network based on the GeoPandas (Jordahl et al. 2019) and Networkx (Hagberg, Schult, and Swart [2010] 2019) libraries;
* Smartgrid: a tool to size the energy systems by minimizing either the GHG emissions, the global cost, or the primary energy consumption based on the PyPSA toolbox (Brown, Hörsch, and Schlachtberger 2018) and the CBC solver (Forrest et al. 2018).
* Workflow: a task organizer based on the Luigi library (Bernhardsson and Freider 2012) to run simulations using the different modules in a comprehensive and automated workflow. 


Citation
---------------------
DESsim is free to use. You are kindly invited to acknowledge its use by citing it in a research paper you are writing, reports, and/or other applicable materials.
Please cite the following paper :
    

    @inproceedings{pouchain_flexible_2020,
    	address = {EuroSun 2020, Athens},
    	title = {A {Flexible} {GIS}-based {Computational} {Framework} for the {Early} {Design} of {District} {Energy} {Networks}},
    	author = {Pouchain, Félix and Peronato, Giuseppe and Meunier, Guillaume},
    	year = {2020}
    }

[You can find this paper this way](https://www.dropbox.com/s/0iy317q5gqrt8jk/article_smartgrid_ISES_v3.pdf?dl=0)



License
---------------------
DESsim 
Copyright (c) 2019-2020, Elioth (Egis Concept), France

DESsim is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version. 
 
DESsim is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License along with DESsim; If not, see <http://www.gnu.org/licenses/>.
 
@license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>


Useful links
---------------------
- [PyPSA ](hhttps://pypsa.org/)


Contributors(a-z):
---------------------
- [Giuseppe Peronato](https://gitlab.com/gperonato)
- [Félix Pouchain](https://gitlab.com/FlxPo)
- [Arnaud Sanson](https://gitlab.com/arno.s)


Acknowledgments
---------------------
The development of these components is supported by Elioth (Egis Concept).
